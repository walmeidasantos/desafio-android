package com.bearapps.concrete_challenge;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.QuickContactBadge;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;


public class ActivityEditor extends Activity {

    private TextView title;
    private ImageView thumbnail;
    private TextView countview;
    private QuickContactBadge photo_author;
    private TextView authname;
    private TextView description;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewitem);

        Intent intent = getIntent();

        title = (TextView) findViewById(R.id.title);
        countview = (TextView) findViewById(R.id.countview);
        authname = (TextView) findViewById(R.id.authname);
        description = (TextView) findViewById(R.id.description);
        thumbnail = (ImageView) findViewById(R.id.thumbnail);
        photo_author = (QuickContactBadge) findViewById(R.id.photo_author);

        Transformation transformation = new RoundedTransformationBuilder()
                .borderColor(Color.BLACK)
                .borderWidthDp(0)
                .cornerRadiusDp(30)
                .oval(true)
                .build();

        Picasso.with(getApplicationContext() ).load(intent.getStringExtra("urlavatar")).transform(transformation).into(photo_author);
        Picasso.with(getApplicationContext() ).load(intent.getStringExtra("urlThumbnail")).into(thumbnail);

        title.setText( intent.getStringExtra("title") );
        countview.setText( intent.getStringExtra("countview") );
        authname.setText(intent.getStringExtra("name"));
        description.setText(intent.getStringExtra("description"));

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
