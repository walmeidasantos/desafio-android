package com.bearapps.concrete_challenge.test;

import com.bearapps.concrete_challenge.MainListActivity;
import com.robotium.solo.*;
import android.test.ActivityInstrumentationTestCase2;


public class test extends ActivityInstrumentationTestCase2<MainListActivity> {
  	private Solo solo;
  	
  	public test() {
		super(MainListActivity.class);
  	}

  	public void setUp() throws Exception {
        super.setUp();
		solo = new Solo(getInstrumentation());
		getActivity();
  	}
  
   	@Override
   	public void tearDown() throws Exception {
        solo.finishOpenedActivities();
        super.tearDown();
  	}
  
	public void testRun() {
        //Wait for activity: 'com.bearapps.concrete_challenge.MainListActivity'
		solo.waitForActivity(com.bearapps.concrete_challenge.MainListActivity.class, 2000);
        //Sleep for 10871 milliseconds
		solo.sleep(10871);
        //Click on 2D Forest Pack 4358
		solo.clickInList(1, 0);
        //Wait for activity: 'com.bearapps.concrete_challenge.ActivityEditor'
		assertTrue("com.bearapps.concrete_challenge.ActivityEditor is not found!", solo.waitForActivity(com.bearapps.concrete_challenge.ActivityEditor.class));
        //Sleep for 3270 milliseconds
		solo.sleep(3270);
        //Press menu back key
		solo.goBack();
        //Sleep for 6808 milliseconds
		solo.sleep(6808);
        //Click on El Capitan - Clipboard history with continuity 1926
		solo.clickOnView(solo.getView(com.bearapps.concrete_challenge.R.id.thumbnail, 1));
        //Wait for activity: 'com.bearapps.concrete_challenge.ActivityEditor'
		assertTrue("com.bearapps.concrete_challenge.ActivityEditor is not found!", solo.waitForActivity(com.bearapps.concrete_challenge.ActivityEditor.class));
        //Sleep for 8295 milliseconds
		solo.sleep(8295);
        //Press menu back key
		solo.goBack();
        //Sleep for 2295 milliseconds
		solo.sleep(2295);
        //Click on Next
		solo.clickOnView(solo.getView(com.bearapps.concrete_challenge.R.id.next));
        //Sleep for 7342 milliseconds
		solo.sleep(7342);
        //Click on A / Astronomy 445
		solo.clickInList(1, 0);
        //Wait for activity: 'com.bearapps.concrete_challenge.ActivityEditor'
		assertTrue("com.bearapps.concrete_challenge.ActivityEditor is not found!", solo.waitForActivity(com.bearapps.concrete_challenge.ActivityEditor.class));
        //Sleep for 5257 milliseconds
		solo.sleep(5257);
        //Press menu back key
		solo.goBack();
        //Sleep for 1091 milliseconds
		solo.sleep(1091);
        //Click on Fancy  DD  Monogram ... 376
		solo.clickInList(2, 0);
        //Wait for activity: 'com.bearapps.concrete_challenge.ActivityEditor'
		assertTrue("com.bearapps.concrete_challenge.ActivityEditor is not found!", solo.waitForActivity(com.bearapps.concrete_challenge.ActivityEditor.class));
        //Sleep for 3396 milliseconds
		solo.sleep(3396);
        //Press menu back key
		solo.goBack();
	}
}
